package com.tom.databasetest.utils;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * Created by Tom on 2017/4/16.
 */

/**
 * 使用这个需要配置权限
 */
public class TestProvider extends ContentProvider {

    public static final int BOOK_DIR = 0;
    public static final int BOOK_ITME = 1;
    public static final int CATEGORY_DIR = 2;
    public static final int CATEGORY_ITEM = 3;
    public static final String AUTHORITY = "com.tom.databasetest.utils.provider";

    private static UriMatcher mUriMatcher;
    private TestDatabaseHelper dbHelper;
    private static final String DB_NAME = "BookStore.db";

    static {
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(AUTHORITY,"book",BOOK_DIR);
        mUriMatcher.addURI(AUTHORITY,"book/#",BOOK_ITME);
        mUriMatcher.addURI(AUTHORITY,"category",CATEGORY_DIR);
        mUriMatcher.addURI(AUTHORITY,"category/#",CATEGORY_ITEM);
    }

    /**
     * 内容提供器对数据库的创建和升级
     *
     * @return boolean 成功或失败
     */
    @Override
    public boolean onCreate() {
        dbHelper = new TestDatabaseHelper(getContext(),DB_NAME,null,1);
        return true;
    }

    /**
     * 查询数据
     *
     * @param uri           指定表
     * @param projection    确定查询哪一列
     * @param selection     约束查询哪一行
     * @param selectionArgs 同上
     * @param sortOrder     排序
     * @return Cursor 对象
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = null;

        switch (mUriMatcher.match(uri)){
            case BOOK_DIR:
                //查询table0所有数据
                cursor = db.query("Book",projection,selection,selectionArgs,null,null,sortOrder);
                break;
            case BOOK_ITME:
                String bookId = uri.getPathSegments().get(1);
                cursor = db.query("Book", projection, "id = ?", new String[] { bookId }, null, null,
                        sortOrder);
                //查询单条数据
                break;
            case CATEGORY_DIR:
                cursor = db.query("Category", projection, selection, selectionArgs, null, null,
                        sortOrder);
                break;
            case CATEGORY_ITEM:
                String categoryId = uri.getPathSegments().get(1);
                cursor = db.query("Category", projection, "id = ?", new String[] { categoryId }, null,
                        null, sortOrder);
                break;
            default:
                break;
        }

        return cursor;
    }

    /**
     * 插入数据
     *
     * @param uri
     * @param values
     * @return 返回新记录地址
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Uri uriReturn = null;

        switch (mUriMatcher.match(uri)){
            case BOOK_DIR:
            case BOOK_ITME:
              long newBookId = db.insert("Book", null, values);
              uriReturn = Uri.parse("content://" + AUTHORITY + "/book/" + newBookId);
                break;
            case CATEGORY_DIR:
            case CATEGORY_ITEM:
                long newCategoryId = db.insert("Category", null, values);
                uriReturn = Uri.parse("content://" + AUTHORITY + "/category/" + newCategoryId);
                break;
            default:
                break;
        }

        return uriReturn;
    }

    /**
     * 更新数据
     *
     * @param uri
     * @param values
     * @param selection
     * @param selectionArgs
     * @return
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int updatedRows = 0;
        switch (mUriMatcher.match(uri)) {
            case BOOK_DIR:
                updatedRows = db.update("Book", values, selection, selectionArgs);
                break;
            case BOOK_ITME:
                String bookId = uri.getPathSegments().get(1);
                updatedRows = db.update("Book", values, "id = ?", new String[] { bookId });
                break;
            case CATEGORY_DIR:
                updatedRows = db.update("Category", values, selection, selectionArgs);
                break;
            case CATEGORY_ITEM:
                String categoryId = uri.getPathSegments().get(1);
                updatedRows = db.update("Category", values, "id = ?", new String[] { categoryId });
                break;
            default:
                break;
        }
        return updatedRows;
    }

    /**
     * 删除数据
     *
     * @param uri
     * @param selection
     * @param selectionArgs
     * @return
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int deletedRows = 0;
        switch (mUriMatcher.match(uri)) {
            case BOOK_DIR:
                deletedRows = db.delete("Book", selection, selectionArgs);
                break;
            case BOOK_ITME:
                String bookId = uri.getPathSegments().get(1);
                deletedRows = db.delete("Book", "id = ?", new String[] { bookId });
                break;
            case CATEGORY_DIR:
                deletedRows = db.delete("Category", selection, selectionArgs);
                break;
            case CATEGORY_ITEM:
                String categoryId = uri.getPathSegments().get(1);
                deletedRows = db.delete("Category", "id = ?", new String[] { categoryId });
                break;
            default:
                break;
        }
        return deletedRows;
    }

    /**
     * 根据传入的uri返回对应的mime
     *
     * @param uri
     * @return
     */
    @Override
    public String getType(Uri uri) {
        switch (mUriMatcher.match(uri)){
            case BOOK_DIR:
                return "vnd.android.cursor.dir/vnd."+AUTHORITY+".book";
            case BOOK_ITME:
                return "vnd.android.cursor.item/vnd."+AUTHORITY+".table0";
            case CATEGORY_DIR:
                return "vnd.android.cursor.dir/vnd."+AUTHORITY+".category";
            case CATEGORY_ITEM:
                return "vnd.android.cursor.item/vnd."+AUTHORITY+".category";
            default:
                break;
        }
        return null;
    }
}
