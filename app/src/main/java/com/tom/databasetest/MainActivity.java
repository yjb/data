package com.tom.databasetest;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.tom.databasetest.utils.TestDatabaseHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button mInsert;
    private Button mUpdate;
    private Button mDelete;
    private Button mQuery;
    private Button mReplace;
    private Button mCreateDate;
    private static final String DB_NAME = "BookStore.db";

    private static final String TAG = "Database";
    private SQLiteDatabase mWriteableDb;

    private TestDatabaseHelper dbHepler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCreateDate = (Button) findViewById(R.id.btn_create);
        mDelete = (Button) findViewById(R.id.btn_delete);
        mInsert = (Button) findViewById(R.id.btn_insert);
        mQuery = (Button) findViewById(R.id.btn_query);
        mReplace = (Button) findViewById(R.id.btn_replace);
        mUpdate = (Button) findViewById(R.id.btn_upate);

        mCreateDate.setOnClickListener(this);
        mDelete.setOnClickListener(this);
        mInsert.setOnClickListener(this);
        mQuery.setOnClickListener(this);
        mReplace.setOnClickListener(this);
        mUpdate.setOnClickListener(this);

        dbHepler = new TestDatabaseHelper(this, DB_NAME, null, 1);
        mWriteableDb = dbHepler.getWritableDatabase();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create:
                dbHepler.getWritableDatabase();
                break;
            case R.id.btn_insert:
                Log.d(TAG, "点击插入数据");

                ContentValues bookValues = new ContentValues();
                bookValues.put("name", "add book name1");
                bookValues.put("author", "Dan Brown");
                bookValues.put("pages", 454);
                bookValues.put("price", 16.96);
                mWriteableDb.insert("Book", null, bookValues);
                bookValues.clear();
                bookValues.put("name", "The Lost Symbol");
                bookValues.put("author", "Dan Brown");
                bookValues.put("pages", 510);
                bookValues.put("price", 19.95);
                mWriteableDb.insert("Book", null, bookValues);
                ContentValues cateGoryValue = new ContentValues();
                cateGoryValue.put("category_name", "test category");
                cateGoryValue.put("category_code", 2);
                mWriteableDb.insert("Category", null, cateGoryValue);

                break;
            case R.id.btn_delete:
                Log.d(TAG, "点击删除");
                mWriteableDb.delete("Book", "pages > ?", new String[]{"500"});
                mWriteableDb.delete("Category","category_code=?",new String[]{"2"});
                break;
            case R.id.btn_query:
                Log.d(TAG, "点击查询");
                Uri uri = Uri.parse("content://com.tom.databasetest.utils.provider/book");
                Cursor cursor = getContentResolver().query(uri,null,null,null,null);
                if(cursor!=null){
                    while (cursor.moveToNext()){
                        String name = cursor.getString(cursor
                                .getColumnIndex("name"));
                        String author = cursor.getString(cursor
                                .getColumnIndex("author"));
                        int pages = cursor.getInt(cursor
                                .getColumnIndex("pages"));
                        double price = cursor.getDouble(cursor
                                .getColumnIndex("price"));
                        Log.d(TAG, "book name is " + name);
                        Log.d(TAG, "book author is " + author);
                        Log.d(TAG, "book pages is " + pages);
                        Log.d(TAG, "book price is " + price);
                    }
                    cursor.close();
                }
                break;
            case R.id.btn_replace:
                Log.d(TAG, "点击替换");
                mWriteableDb.beginTransaction();

                try {
                   mWriteableDb.delete("Book", null, null);
                    ContentValues replaceValues = new ContentValues();
                    replaceValues.put("name", "update Game of Thrones");
                    replaceValues.put("author", "update George Martin");
                    replaceValues.put("pages", 720);
                    replaceValues.put("price", 20.85);
                    mWriteableDb.insert("Book", null, replaceValues);
                    mWriteableDb.setTransactionSuccessful();
                }catch (Exception e){
                    Log.e(TAG,"出错了");
                   e.printStackTrace();
                }finally {
                   mWriteableDb.endTransaction();
                }
                break;
            case R.id.btn_upate:
                Log.d(TAG, "点击修改");
                ContentValues updateValues = new ContentValues();
                updateValues.put("price", 1.99);
                mWriteableDb.update("Book", updateValues, "name = ?",
                        new String[] { "The Lost Symbol" });
                break;
        }
    }
}
